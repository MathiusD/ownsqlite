default_target: tests

clean:
	@git clean -f tests/*.sqlite3
	@git checkout tests/*.sqlite3

test:
	@python3 -m unittest

tests: clean test clean