import unittest
from own.own_sqlite.keys import *
from own.own_sqlite.fields import Id, FieldRef

class Test_Own_SQLite_Keys(unittest.TestCase):

    def test_key(self):
        key1 = Key(Id)
        self.assertEqual(key1.field, Id)
    
    def test_primary_key(self):
        out_key1 = {"column":Id.name, "Auto_Increment":"true"}
        out_key2 = {"column":Id.name, "Auto_Increment":"false"}
        key1 = PrimaryKey(Id, True)
        key2 = PrimaryKey(Id)
        self.assertEqual(out_key1, key1.pattern)
        self.assertEqual(out_key2, key2.pattern)

    def test_foreign_key(self):
        ref1 = FieldRef(Id, "BriBrup")
        ref2 = FieldRef(Id, "Blap")
        out_key1 = {"column":Id.name,"cible":ref1.pattern}
        out_key2 = {"column":Id.name,"cible":ref2.pattern}
        key1 = ForeignKey(Id, ref1)
        key2 = ForeignKey(Id, ref2)
        self.assertEqual(out_key1, key1.pattern)
        self.assertEqual(out_key2, key2.pattern)