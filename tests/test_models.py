import unittest, os
from own.own_sqlite.models import *
from own.own_sqlite.keys import *
from own.own_sqlite.fields import *
from own.own_sqlite import *

class Test_Own_SQLite_Models(unittest.TestCase):

    def test_model_pattern(self):
        model1 = Model("Simple ID")
        fields = [Id, Text("Stalker")]
        model2 = Model("Stalk", fields)
        ownfields = [Integer("OwnID", True), Text("OwnText"), Integer("OwnOtherID", True)]
        ownpkey = PrimaryKey(ownfields[0], True)
        ownfkeys = [ForeignKey(ownfields[2], FieldRef(Id, model2.name))]
        ownmodel = Model("OwnTable", ownfields, ownpkey, ownfkeys)
        out_model1_creation = {
            "data":[Id.pattern],
            "primary_key":Default_PrimaryKey.pattern,
            "foreign_key":[]
        }
        out_model2_creation = {
            "data":[Id.pattern, fields[1].pattern],
            "primary_key":Default_PrimaryKey.pattern,
            "foreign_key":[]
        }
        out_ownmodel_creation = {
            "data":[],
            "primary_key":ownpkey.pattern,
            "foreign_key":[]
        }
        for field in ownfields:
            out_ownmodel_creation["data"].append(field.pattern)
        for fkey in ownfkeys:
            out_ownmodel_creation["foreign_key"].append(fkey.pattern)
        self.assertEqual(model1._CreationPattern, out_model1_creation)
        self.assertEqual(model2._CreationPattern, out_model2_creation)
        self.assertEqual(ownmodel._CreationPattern, out_ownmodel_creation)
        out_model1_insertion = {
            "table":model1.name,
            "data":[],
            "string":[],
            "cp":Default_PrimaryKey.field.name
        }
        out_model2_insertion = {
            "table":model2.name,
            "data":[],
            "string":[],
            "cp":Default_PrimaryKey.field.name
        }
        out_ownmodel_insertion = {
            "table":ownmodel.name,
            "data":[ownfields[2].name, ownfields[1].name],
            "string":[ownfields[1].name],
            "cp":ownpkey.field.name
        }
        self.assertEqual(model1._generateInsertionPattern(), out_model1_insertion)
        self.assertEqual(model2._generateInsertionPattern(), out_model2_insertion)
        self.assertEqual(ownmodel._generateInsertionPattern([ownfields[1]]), out_ownmodel_insertion)
        out_model1_update = {
            "table":model1.name,
            "data":[Id.name],
            "string":[],
            "cp":Default_PrimaryKey.field.name
        }
        out_model2_update = {
            "table":model2.name,
            "data":[Id.name],
            "string":[],
            "cp":Default_PrimaryKey.field.name
        }
        out_ownmodel_update = {
            "table":ownmodel.name,
            "data":[ownfields[0].name, ownfields[2].name, ownfields[1].name],
            "string":[ownfields[1].name],
            "cp":ownpkey.field.name
        }
        self.assertEqual(model1._generateUpdatePattern(), out_model1_update)
        self.assertEqual(model2._generateUpdatePattern(), out_model2_update)
        self.assertEqual(ownmodel._generateUpdatePattern([ownfields[1]]), out_ownmodel_update)
        out_model1_search_empty = {
            "column":"id",
            "table":model1.name,
            "option":[],
            "seperator_option":" AND "
        }
        out_model2_search_empty  = {
            "column":"id, Stalker",
            "table":model2.name,
            "option":[],
            "seperator_option":" AND "
        }
        out_ownmodel_search_empty  = {
            "column":"OwnID, OwnText, OwnOtherID",
            "table":ownmodel.name,
            "option":[],
            "seperator_option":" AND "
        }
        self.assertEqual(model1._generateSearchPattern(), out_model1_search_empty)
        self.assertEqual(model2._generateSearchPattern(), out_model2_search_empty)
        self.assertEqual(ownmodel._generateSearchPattern(), out_ownmodel_search_empty)
        bisFields = [Id, TextWithoutCaseSensitive("Stalker")]
        model2bis = Model("Stalk", bisFields)
        model1_arg = [
            {"data":9,"field":Id}
        ]
        model2_arg = [
            {"data":"Blap", "field":model2.fields[1]}
        ]
        model2bis_arg = [
            {"data":"Blap", "field":model2bis.fields[1]}
        ]
        ownmodel_arg = [
            {"data":45,"field":ownmodel.fields[2]},
            {"data":"Blap","field":ownmodel.fields[1]}
        ]
        out_model1_search = {
            "column":"id",
            "table":model1.name,
            "option":[
                {"column":"id","data":{"format":"","criteria":9,"check":True,"type_key":int}}
            ],
            "seperator_option":" AND "
        }
        out_model2_search  = {
            "column":"id, Stalker",
            "table":model2.name,
            "option":[
                {"column":"Stalker","data":{"format":"","criteria":"Blap","check":True,"type_key":str}}
            ],
            "seperator_option":" AND "
        }
        out_model3_search  = {
            "column":"id, Stalker",
            "table":model2.name,
            "option":[
                {"column":"Stalker","data":{"format":"","criteria":"\"Blap\" COLLATE NOCASE","check":False,"type_key":str}}
            ],
            "seperator_option":" AND "
        }
        out_ownmodel_search  = {
            "column":"OwnText, OwnOtherID",
            "table":ownmodel.name,
            "option":[
                {"column":"OwnOtherID","data":{"format":"","criteria":45,"check":True,"type_key":int}},
                {"column":"OwnText","data":{"format":"","criteria":"Blap","check":True,"type_key":str}}
            ],
            "seperator_option":" AND "
        }
        self.assertEqual(model1._generateSearchPattern(model1_arg), out_model1_search)
        self.assertEqual(model2._generateSearchPattern(model2_arg), out_model2_search)
        self.assertEqual(model2bis._generateSearchPattern(model2bis_arg), out_model3_search)
        self.assertEqual(ownmodel._generateSearchPattern(ownmodel_arg, [ownfields[1], ownfields[2]]), out_ownmodel_search)
        out_model1_del = {
            "table":model1.name,
            "option":[
                {"column":"id","data":{"format":"","criteria":9,"check":True,"type_key":int}}
            ],
            "seperator_option":" AND "
        }
        out_model2_del  = {
            "table":model2.name,
            "option":[
                {"column":"Stalker","data":{"format":"","criteria":"Blap","check":True,"type_key":str}}
            ],
            "seperator_option":" AND "
        }
        out_model3_del  = {
            "table":model2.name,
            "option":[
                {"column":"Stalker","data":{"format":"","criteria":"\"Blap\" COLLATE NOCASE","check":False,"type_key":str}}
            ],
            "seperator_option":" AND "
        }
        out_ownmodel_del  = {
            "table":ownmodel.name,
            "option":[
                {"column":"OwnOtherID","data":{"format":"","criteria":45,"check":True,"type_key":int}},
                {"column":"OwnText","data":{"format":"","criteria":"Blap","check":True,"type_key":str}}
            ],
            "seperator_option":" AND "
        }
        self.assertEqual(model1._generateDelPattern(model1_arg), out_model1_del)
        self.assertEqual(model2._generateDelPattern(model2_arg), out_model2_del)
        self.assertEqual(model2bis._generateDelPattern(model2bis_arg), out_model3_del)
        self.assertEqual(ownmodel._generateDelPattern(ownmodel_arg), out_ownmodel_del)

    def test_model_db(self):
        path = "tests/models.sqlite3"
        db = connect(path)
        fields = [Id, Text("Stalker")]
        model = Model("Stalk", fields)
        self.assertEqual(False, table_exist(model.name, db))
        model.create(db)
        self.assertNotEqual(model, None)
        self.assertEqual(True, table_exist(model.name, db))
        ownfields = [Integer("OwnID", True), Text("OwnText"), Integer("OwnOtherID", True)]
        ownpkey = PrimaryKey(ownfields[0], True)
        ownfkeys = [ForeignKey(ownfields[2], FieldRef(Id, model.name))]
        self.assertEqual(False, table_exist("OwnTable", db))
        ownmodel = Model("OwnTable", ownfields, ownpkey, ownfkeys, db)
        self.assertEqual(True, table_exist(ownmodel.name, db))
        data1 = ModelData(model.fields, [None, "Gloup"])
        data2 = ModelData(model.fields, [None, "Grip"])
        owndata1 = ModelData(ownmodel.fields, [None, "Blap", 1])
        owndata2 = ModelData(ownmodel.fields, [None, "Brip", 1])
        owndata3 = ModelData(ownmodel.fields, [None, "'Lut'", 1])
        owndatalist1 = [owndata1, owndata2, owndata3]
        owndatalistresults = [
            ModelData(ownmodel.fields, [1, "Blap", 1]),
            ModelData(ownmodel.fields, [2, "Brip", 1]),
            ModelData(ownmodel.fields, [3, "'Lut'", 1])
        ]
        self.assertEqual(ModelData(model.fields, [1, "Gloup"]).pattern, model.insertOne(data1, db).pattern)
        self.assertEqual(ModelData(model.fields, [1, data1.Stalker]).pattern, model.fetchAll(db)[0].pattern)
        results = ownmodel.insertMany(owndatalist1, db, True)
        for result in results:
            self.assertEqual(owndatalistresults[results.index(result)].pattern, result.pattern)
        self.assertEqual(ModelData(model.fields, [2, "Grip"]).pattern, model.insertOne(data2, db).pattern)
        data3 = ModelData(model.fields, [1, "..."])
        owndata5 = ModelData(ownmodel.fields, [1, "'Lut'", 1])
        owndata6 = ModelData(ownmodel.fields, [18, "POuet", 6])
        owndatalist2 = [owndata5, owndata6]
        self.assertEqual(data3, model.updateOne(data3, db))
        self.assertEqual(owndatalist2, ownmodel.updateMany(owndatalist2, db, True))
        self.assertEqual(True, ownmodel.updateMany(owndatalist2, db))
        self.assertEqual(data3.pattern, model.fetchOneByField((1), Id, db).pattern)
        owndata3_temp = owndata3
        owndata3_temp.OwnID = 3
        ownverif = [
            owndata5.pattern,
            owndata3_temp.pattern
        ]
        out = ownmodel.fetchByField("'Lut'", ownmodel.fields[1], db)
        outverif = []
        for o in out:
            outverif.append(o.pattern)
        self.assertEqual(ownverif, outverif)
        self.assertEqual(True, ownmodel.DelByField("'Lut'", ownmodel.fields[1], db))
        self.assertEqual([], ownmodel.fetchByField("'Lut'", ownmodel.fields[1], db))
        self.assertEqual(True, ownmodel.delAll(db))
        self.assertEqual([], ownmodel.fetchAll(db))
        owndata7 = ModelData(ownmodel.fields, [None, None, 1])
        owndata8 = ModelData(ownmodel.fields, [None, None, 1])
        owndatalist3 = [owndata7, owndata8]
        self.assertEqual(True, ownmodel.insertMany(owndatalist3, db))
        self.assertEqual(False, Model.checkInsertUpdateMany(None))
        self.assertEqual(len(ownmodel.fetchAll(db)), len(ownmodel.fetchByField(0, ownmodel.fields[0], db, ">=")))
        os.remove(path)