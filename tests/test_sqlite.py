import unittest, os
from own.own_sqlite import *

class Test_Own_SQLite(unittest.TestCase):

    def test_connect(self):
        path = "tests/test.sqlite3"
        Verif = False
        try:
            connect(path)
            Verif = True
        finally:
            self.assertEqual(True, Verif)

    def test_bdd_exist(self):
        path1 = "tests/test.sqlite3"
        path2 = "tests/test.csv"
        path3 = "tests/.test.sqlite3"
        self.assertEqual(True, bdd_exist(path1))
        self.assertEqual(False, bdd_exist(path2))
        self.assertEqual(False, bdd_exist(path3))

    def test_bdd_create(self):
        path = "tests/test_.sqlite3"
        self.assertEqual(True, bdd_create(path))
        self.assertEqual(False, bdd_create(path))
        os.remove(path)
        self.assertEqual(True, bdd_create(path))
        os.remove(path)

    def test_module_bdd(self):
        path = "tests/test_.sqlite3"
        self.assertEqual(False, bdd_exist(path))
        verif = bdd_create(path)
        self.assertEqual(verif, bdd_exist(path))
        os.remove(path)

    def test_table_exist(self):
        path = "tests/test.sqlite3"
        table1 = "Trains"
        table2 = "Example_1"
        bdd = connect(path)
        self.assertEqual(True, table_exist(table1, bdd))
        self.assertEqual(False, table_exist(table2, bdd))

    def test_table_create(self):
        path = "tests/test.sqlite3"
        table1 = "Trains"
        table2 = "Example"
        table3 = "ndExample"
        table4 = "Third"
        columns1 = {
            "data" :[
                {"name":"Ref", "type":"INTEGER", "not_null":"false"},
                {"name":"Like", "type":"INTEGER", "not_null":"false"},
                {"name":"Nb_Death", "type":"INTEGER", "not_null":"false"}
            ],
            "primary_key":{"column":"Ref", "Auto_Increment":"false"},
            "foreign_key":[]
        }
        columns2 = {
            "data" :[
                {"name":"data1", "type":"INTEGER", "not_null":"true"},
                {"name":"data2", "type":"TEXT", "not_null":"false"}
            ],
            "primary_key":{"column":"data1", "Auto_Increment":"true"},
            "foreign_key":[]
        }
        columns3 = {
            "data" :[
                {"name":"data1", "type":"INTEGER", "not_null":"true"},
                {"name":"data2", "type":"TEXT", "not_null":"false"},
                {"name":"ref_example", "type":"INTEGER", "not_null":"true"}
            ],
            "primary_key":{"column":"data1", "Auto_Increment":"true"},
            "foreign_key":[
                {
                    "column":"ref_example",
                    "cible":{"table":"Example", "column":"data1"}
                }
            ]
        }
        columns4 = {
            "data" :[
                {"name":"data1", "type":"INTEGER", "not_null":"true"},
                {"name":"ref_example_1", "type":"TEXT", "not_null":"true"},
                {"name":"ref_example_2", "type":"INTEGER", "not_null":"true"}
            ],
            "primary_key":{"column":"data1", "Auto_Increment":"true"},
            "foreign_key":[
                {
                    "column":"ref_example_1",
                    "cible":{"table":"Example", "column":"data2"}
                },
                {
                    "column":"ref_example_2",
                    "cible":{"table":"ndExample", "column":"data1"}
                }
            ]
        }
        schema2 = 'CREATE TABLE Example("data1" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"data2" TEXT)'
        schema3 = 'CREATE TABLE ndExample("data1" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"data2" TEXT,"ref_example" INTEGER NOT NULL,FOREIGN KEY("ref_example") REFERENCES "Example"("data1"))'
        schema4 = 'CREATE TABLE Third("data1" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"ref_example_1" TEXT NOT NULL,"ref_example_2" INTEGER NOT NULL,FOREIGN KEY("ref_example_1") REFERENCES "Example"("data2"),FOREIGN KEY("ref_example_2") REFERENCES "ndExample"("data1"))'
        bdd = connect(path)
        self.assertEqual(False, table_create(table1, columns1, bdd))
        self.assertEqual(True, table_create(table2, columns2, bdd))
        self.assertEqual(True, table_create(table3, columns3, bdd))
        self.assertEqual(True, table_create(table4, columns4, bdd))
        cursor = bdd.cursor()
        cursor.execute('''SELECT sql FROM sqlite_master WHERE name = 'Example' ''')
        bdd.commit()
        data = cursor.fetchall()
        self.assertEqual(schema2, data[0][0])
        cursor.execute('''SELECT sql FROM sqlite_master WHERE name = 'ndExample' ''')
        bdd.commit()
        data = cursor.fetchall()
        self.assertEqual(schema3, data[0][0])
        cursor.execute('''SELECT sql FROM sqlite_master WHERE name = 'Third' ''')
        bdd.commit()
        data = cursor.fetchall()
        self.assertEqual(schema4, data[0][0])
        cursor.execute('''DROP TABLE Example''')
        cursor.execute('''DROP TABLE ndExample''')
        cursor.execute('''DROP TABLE Third''')
        bdd.commit()
        bdd.close()

    def test_extract_table(self):
        tableTrains = {
            "data": [
                {
                    "name": "Ref",
                    "type": "INT",
                    "not_null": False
                },
                {
                    "name": "Like",
                    "type": "INT",
                    "not_null": False
                },
                {
                    "name": "Nb_Death",
                    "type": "INT",
                    "not_null": False
                }
            ],
            "primary_key": {
                "column": "Ref",
                "Auto_Increment": True
            },
            "foreign_key": []
        }
        path = "tests/test.sqlite3"
        bdd = connect(path)
        self.assertEqual(tableTrains, extract_table("Trains", bdd))
        self.assertEqual(None, extract_table("Patate", bdd))

    def test_module_table(self):
        path = "tests/test.sqlite3"
        table = "Example_2"
        columns = {
            "data" :[
                {"name":"data1", "type":"INTEGER", "not_null":"true"},
                {"name":"data2", "type":"TEXT", "not_null":"false"}
            ],
            "primary_key":{"column":"data1", "Auto_Increment":"true"},
            "foreign_key":[]
        }
        bdd = connect(path)
        self.assertEqual(False, table_exist(table, bdd))
        verif = table_create(table, columns, bdd)
        self.assertEqual(verif, table_exist(table, bdd))
        if verif == True:
            cursor = bdd.cursor()
            cursor.execute('''DROP TABLE Example_2''')
            bdd.commit()
        bdd.close()

    def test_data_insert_data_in_req(self):
        req = "Blap"
        data1 = None
        data2 = "Bloup"
        data3 = 45
        self.assertEqual(use_data_in_req(req, data1), "BlapNULL")
        self.assertEqual(use_data_in_req(req, data2), "Blap\"Bloup\"")
        self.assertEqual(use_data_in_req(req, data3), "Blap45")

    def test_data_exist(self):
        path = 'tests/test.sqlite3'
        search1 = 1
        search2 = 665
        search3 = (1, 99, 666)
        search_pattern1 = {
            "column":"Ref",
            "table":"Trains",
            "option":[
                {"column":"Ref","data":{"format":"search_input","criteria":""}},
                {"column":"Nb_death","data":{"format":"","criteria":666,"type_key":int}}
            ],
            "seperator_option":" AND ",
            "type_key":int
        }
        search_pattern2 = {
            "column":"Nb_death",
            "table":"Trains",
            "option":[],
            "seperator_option":"",
            "type_key":int
        }
        search_pattern3 = {
            "column":"*",
            "table":"Trains",
            "option":[],
            "seperator_option":"",
            "type_key":tuple
        }
        bdd = connect(path)
        self.assertEqual(True, data_exist(search1, search_pattern1, bdd))
        self.assertEqual(False, data_exist(search2, search_pattern2, bdd))
        self.assertEqual(True, data_exist(search3, search_pattern3, bdd, False))

    def test_data_extract(self):
        path = 'tests/test.sqlite3'
        search1 = 1
        search2 = (1, 99, 666)
        search_pattern1 = {
            "column":"*",
            "table":"Trains",
            "option":[
                {"column":"Ref","data":{"format":"search_input","criteria":""}},
                {"column":"Nb_death","data":{"format":"","criteria":666,"type_key":int}}
            ],
            "seperator_option":" AND ",
            "type_key":int
        }
        search_pattern2 = {
            "column":"*",
            "table":"Trains",
            "option":[],
            "seperator_option":"",
            "type_key":tuple
        }
        bdd = connect(path)
        self.assertEqual((1, 99, 666), data_extract(search1, search_pattern1, bdd, False))
        self.assertEqual([(1, 99, 666)], data_extract(search2, search_pattern2, bdd))

    def test_data_insert(self):
        path = 'tests/test.sqlite3'
        data1 = {"Like": 10}
        data2 = {"Nb_Death":"Blip"}
        data3 = {"Like": 10}
        pattern1 = {
            "table":"Trains",
            "data":[
                "Like"
            ],
            "string":[]
        }
        pattern2 = {
            "table":"Trains",
            "data":[
                "Nb_Death"
            ],
            "string":[]
        }
        pattern3 = {
            "table":"Trains",
            "data":[
                "Like"
            ],
            "cp":"Ref",
            "string":[]
        }
        result1 = (2, 10)
        bdd = connect(path)
        self.assertEqual(True , data_insert(data1, pattern1, bdd))
        cursor = bdd.cursor()
        cursor.execute('''SELECT Ref, Like FROM Trains WHERE Ref=2''')
        bdd.commit()
        data = cursor.fetchall()
        self.assertEqual(1, len(data))
        self.assertEqual(data[0], result1)
        cursor.execute('''DELETE FROM Trains Where Ref=2''')
        bdd.commit()
        self.assertEqual(False, data_insert(data2, pattern2, bdd))
        self.assertEqual((3, 10, None), data_insert(data3, pattern3, bdd))
        bdd.close()

    def test_data_delete(self):
        path = 'tests/test.sqlite3'
        data = 9
        pattern = {
            "table":"Trains",
            "option":[
                {"column":"Ref","data":{"format":"search_input","criteria":""}},
            ],
            "seperator_option":" AND ",
            "type_key":int
        }
        bdd = connect(path)
        self.assertEqual(True, data_remove(data, pattern, bdd))
        cursor = bdd.cursor()
        cursor.execute('''SELECT Ref, Like FROM Trains WHERE Ref=2''')
        bdd.commit()
        self.assertEqual(cursor.fetchall(), [])

    def test_data_update(self):
        path = 'tests/test.sqlite3'
        data1 = {"Ref": 1, "Like": 10,}
        data2 = {"Ref": 85, "Nb_Death": 666}
        pattern1 = {
            "table":"Trains",
            "data":[
                "Like"
            ],
            "string":[],
            "cp":"Ref"
        }
        pattern2 = {
            "table":"Trains",
            "data":[
                "Nb_Death"
            ],
            "string":[],
            "cp":"Ref"
        }
        result1 = (1, 10, 666)
        bdd = connect(path)
        self.assertEqual(True, data_update(data1, pattern1, bdd))
        cursor = bdd.cursor()
        cursor.execute('''SELECT * FROM Trains WHERE Ref=1''')
        bdd.commit()
        data = cursor.fetchall()
        self.assertEqual(data[0], result1)
        cursor.execute('''UPDATE Trains SET Like=99 WHERE Ref=1''')
        bdd.commit()
        self.assertEqual(True, data_update(data2, pattern2, bdd))
        bdd.close()

    def test_module_data(self):
        path = 'tests/test.sqlite3'
        search = 9
        search_pattern = {
            "column":"Ref",
            "table":"Trains",
            "option":[],
            "seperator_option":"",
            "type_key":int
        }
        data = {"Ref": 9, "Like": 10, "Nb_Death":69}
        pattern = {
            "table":"Trains",
            "data":[
                "Ref",
                "Like",
                "Nb_Death"
            ],
            "string":[],
            "cp":"Ref"
        }
        data_value = [(1,), (9,)]
        bdd = connect(path)
        self.assertEqual(False, data_exist(search, search_pattern, bdd))
        verif = True if data_insert(data, pattern, bdd) is not None else False
        self.assertEqual(verif, data_exist(search, search_pattern, bdd))
        if verif == True :
            if (data_extract(search, search_pattern, bdd) == data_value):
                data = {"Ref": 2, "Like": 45, "Nb_Death":666}
                verif_2 = data_update(data, pattern, bdd)
                if verif_2 == True:
                    result = (2, 45, 666)
                else:
                    result = (2, 10, 69)
                cursor = bdd.cursor()
                cursor.execute('''SELECT * FROM Trains WHERE Ref=2''')
                bdd.commit()
                data = cursor.fetchall()
                self.assertEqual(data[0], result)
                cursor.execute('''DELETE FROM Trains Where Ref=2''')
                bdd.commit()
        bdd.close()

    def test_module_sqlite(self):
        path = "tests/test_2.sqlite3"
        self.assertEqual(False, bdd_exist(path))
        verif_bdd = bdd_create(path)
        self.assertEqual(verif_bdd, bdd_exist(path))
        if verif_bdd == True:
            
            table = "Example_3"
            columns = {
                "data" :[
                    {"name":"data1", "type":"INTEGER", "not_null":"true"},
                    {"name":"data2", "type":"TEXT", "not_null":"false"}
                ],
                "primary_key":{"column":"data1", "Auto_Increment":"true"},
                "foreign_key":[]
            }
            bdd = connect(path)
            self.assertEqual(False, table_exist(table, bdd))
            verif_table = table_create(table, columns, bdd)
            self.assertEqual(verif_table, table_exist(table, bdd))
            if verif_table == True:
                cursor = bdd.cursor()
                search = 1
                search_pattern = {
                    "column":"data1",
                    "table":"Example_3",
                    "option":[],
                    "seperator_option":"",
                    "type_key":int
                }
                data = {"data1": 1, "data2": "Blap"}
                pattern = {
                    "table":"Example_3",
                    "data":[
                        "data1",
                        "data2"
                    ],
                    "string":[
                        "data2"
                    ],
                    "cp":"data1"
                }
                data_value = [(1,)]
                self.assertEqual(False, data_exist(search, search_pattern, bdd))
                verif_data = True if data_insert(data, pattern, bdd) is not None else False
                self.assertEqual(verif_data, data_exist(search, search_pattern, bdd))
                if verif_data == True :
                    if (data_value == data_extract(search, search_pattern, bdd)):
                        data = {"data1": 1, "data2": "Blip"}
                        verif_data_2 = data_update(data, pattern, bdd)
                        if verif_data_2 == True:
                            result = (1, "Blip")
                        else:
                            result = (1, "Blap")
                        cursor = bdd.cursor()
                        cursor.execute('''SELECT * FROM Example_3 WHERE data1=1''')
                        bdd.commit()
                        data = cursor.fetchall()
                        self.assertEqual(data[0], result)
                        cursor.execute('''DELETE FROM Example_3 Where data1=1''')
                        bdd.commit()
                cursor.execute('''DROP TABLE Example_3''')
                bdd.commit()
            bdd.close()
            os.remove(path)

    def test_xyz_clear_after_tests(self):
        path = "tests/test.sqlite3"
        bdd = connect(path)
        cursor = bdd.cursor()
        cursor.execute('''DELETE FROM Trains WHERE Ref IN (9, 3);''')
        bdd.commit()
        cursor.execute('''UPDATE "main"."sqlite_sequence" SET "seq"=1 WHERE "_rowid_"='1';''')
        bdd.commit()
        bdd.close()
