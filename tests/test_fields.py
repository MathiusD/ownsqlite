import unittest
from own.own_sqlite.fields import *

class Test_Own_SQLite_Fields(unittest.TestCase):

    def test_field(self):
        out_field1 = {"name":"Bloup", "type":"Broup", "not_null":"false"}
        out_field2 = {"name":"Salut", "type":"INTEGER", "not_null":"true"}
        field1 = Field("Bloup", str, "Broup")
        field2 = Field("Salut", str, "INTEGER", True)
        self.assertEqual(field1.pattern, out_field1)
        self.assertEqual(field2.pattern, out_field2)

    def test_field_integer(self):
        out_field1 = {"name":"Bloup", "type":"INTEGER", "not_null":"false"}
        out_field2 = {"name":"Salut", "type":"INTEGER", "not_null":"true"}
        field1 = Integer("Bloup")
        field2 = Integer("Salut", True)
        self.assertEqual(field1.pattern, out_field1)
        self.assertEqual(field2.pattern, out_field2)

    def test_field_text(self):
        out_field1 = {"name":"Bloup", "type":"TEXT", "not_null":"false"}
        out_field2 = {"name":"Salut", "type":"TEXT", "not_null":"true"}
        field1 = Text("Bloup")
        field2 = Text("Salut", True)
        self.assertEqual(field1.pattern, out_field1)
        self.assertEqual(field2.pattern, out_field2)

    def test_field_boolean(self):
        out_field1 = {"name":"Bloup", "type":"BOOLEAN", "not_null":"false"}
        out_field2 = {"name":"Salut", "type":"BOOLEAN", "not_null":"true"}
        field1 = Boolean("Bloup")
        field2 = Boolean("Salut", True)
        self.assertEqual(field1.pattern, out_field1)
        self.assertEqual(field2.pattern, out_field2)
    
    def test_field_ref(self):
        out_field1 = {"table":"Bloup", "column":"id"}
        out_field2 = {"table":"BriBrup", "column":"Salut"}
        fieldref1 = FieldRef(Integer("id", True), "Bloup")
        fieldref2 = FieldRef(Text("Salut"), "BriBrup")
        self.assertEqual(fieldref1.pattern, out_field1)
        self.assertEqual(fieldref2.pattern, out_field2)