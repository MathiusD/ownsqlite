from .field import Field

class Boolean(Field):
    """
    Classe représentant un champs de type Boolean dans une base de données.
    Cette classe permet de stocker les diverses informations liées à un
    champs et permet de sortir le pattern qui est requis au sein des
    fonctions du modules afin de spécifier le schéma de la base.
    """

    def __init__(self, name:str, required:bool = False, specific_search:bool = None):
        """
        Cela permet d'instancier la représentation d'un champ de type Boolean.
        Celui-ci requiert un nom de champs et si le champs est requis.
        """
        super().__init__(name, bool, "BOOLEAN", required, specific_search)