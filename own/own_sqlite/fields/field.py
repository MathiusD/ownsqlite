class Field:
    """
    Classe représentant un champs dans une base de données.
    Cette classe permet de stocker les diverses informations liées à un
    champs et permet de sortir le pattern qui est requis au sein des
    fonctions du modules afin de spécifier le schéma de la base.
    """

    def __init__(self, name:str, data_type:type, data_type_txt:str, required:bool = False, specific_search:bool = None):
        """
        Cela permet d'instancier la représentation d'un champ.
        Celui-ci requiert un nom de champ, un type de données
        (par exemple le type int de Python), un type de données
        [plus précisément le nom demandé par SQLite]
        (Ici ceux de SQLite3 tel que "INTEGER"), et enfin si le champs
        est requis.
        """
        self.name = name
        self.data_type = data_type
        self.data_type_txt = data_type_txt
        self.required = required
        self.specific_search = specific_search

    @property
    def pattern(self):
        """
        Cela permet d'obtenir le dictionnaire représentant le champ en base.
        """
        return {"name":self.name, "type":self.data_type_txt, "not_null":str(self.required).lower()}