from .field import Field

class FieldRef:
    """
    Classe représentant une liaison dans une base de données.
    Cette classe permet de stocker les diverses informations liées à une
    liaison et permet de sortir le pattern qui est requis au sein des
    fonctions du modules afin de spécifier le schéma de la base.
    """

    def __init__(self, field:Field, table:str):
        """
        Cela permet d'instancier la représentation d'une liaison.
        Celle-ci requiert un Champ instancié et le nom de la table dont il
        fait partie.
        """
        self.field = field
        self.table = table

    @property
    def pattern(self):
        """
        Cela permet d'obtenir le dictionnaire représentant la liaison en base.
        """
        return {"table":self.table, "column":self.field.name}