from .field import Field

class Text(Field):
    """
    Classe représentant un champs de type Text dans une base de données.
    Cette classe permet de stocker les diverses informations liées à un
    champs et permet de sortir le pattern qui est requis au sein des
    fonctions du modules afin de spécifier le schéma de la base.
    """

    def __init__(self, name:str, required:bool = False, specific_search:bool = None):
        """
        Cela permet d'instancier la représentation d'un champ de type Text.
        Celui-ci requiert un nom de champs et si le champs est requis.
        """
        super().__init__(name, str, "TEXT", required, specific_search)

class TextWithoutCaseSensitive(Text):

    def __init__(self, name:str, required:bool = False):
        super().__init__(name, required=required, specific_search="COLLATE NOCASE")