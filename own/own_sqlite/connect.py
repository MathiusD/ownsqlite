import sqlite3

def connect(path:str):
    """
    Fonction qui nous renvoie l'objet de connexion vers
    une base de données sqlite passé en argument\n
    Entrée :\n
    - path: chaine de caractère indiquant le chemin vers
    la base de données\n
    Sortie :\n
    - bdd : l'objet permettant de communiquer avec la base
    """
    bdd = sqlite3.connect(path)
    return bdd