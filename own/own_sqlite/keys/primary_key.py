from .key import Key
from ..fields import Field

class PrimaryKey(Key):
    """
    Classe représentant une clé primaire dans une base de données.
    Cette classe permet de stocker les diverses informations liées à une
    clé primaire et permet de sortir le pattern qui est requis au sein des
    fonctions du modules afin de spécifier le schéma de la base.
    """

    def __init__(self, field:Field, auto_increment:bool = False):
        """
        Cela permet d'instancier la représentation d'une clé primaire.
        Celle-ci requiert un Champ instancié. Vous pouvez également spécifier si
        cette clé s'incrémente automatiquement ou non.
        """
        super().__init__(field)
        self.auto_increment = auto_increment

    @property
    def pattern(self):
        """
        Cela permet d'obtenir le dictionnaire représentant la clé primaire en base.
        """
        return {"column":self.field.name, "Auto_Increment":str(self.auto_increment).lower()}