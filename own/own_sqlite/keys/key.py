from ..fields import Field

class Key:
    """
    Classe représentant une clé dans une base de données.
    """

    def __init__(self, field:Field):
        """
        Cela permet d'instancier la représentation d'une clé.
        Celle-ci requiert un Champ instancié.
        """
        self.field = field