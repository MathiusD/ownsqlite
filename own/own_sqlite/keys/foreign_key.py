from .key import Key
from ..fields import Field, FieldRef

class ForeignKey(Key):
    """
    Classe représentant une clé secondaire dans une base de données.
    Cette classe permet de stocker les diverses informations liées à une
    clé secondaire et permet de sortir le pattern qui est requis au sein des
    fonctions du modules afin de spécifier le schéma de la base.
    """

    def __init__(self, field:Field, target:FieldRef):
        """
        Cela permet d'instancier la représentation d'une clé secondaire.
        Celle-ci requiert un Champ instancié ainsi que le champ cible
        dans la table tierce.
        """
        super().__init__(field)
        self.target = target

    @property
    def pattern(self):
        """
        Cela permet d'obtenir le dictionnaire représentant la clé secondaire en base.
        """
        return {"column":self.field.name,"cible":self.target.pattern}