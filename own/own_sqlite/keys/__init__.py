from ..fields import Id
from .key import Key
from .primary_key import PrimaryKey
from .foreign_key import ForeignKey
Default_PrimaryKey = PrimaryKey(Id, True)