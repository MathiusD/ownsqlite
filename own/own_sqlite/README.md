# Module own_sqlite

![pipeline](https://gitlab.com/MathiusD/ownsqlite/badges/master/pipeline.svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/d83168ee23aa490db364e1f25f3243b4)](https://www.codacy.com/manual/MathiusD/ownsqlite?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/ownsqlite&amp;utm_campaign=Badge_Grade)

## Description

Ce module nous permet de manipuler des bases de données à travers 3 sous modules :

* connect qui nous permet de nous connecter à une base
* bdd qui gère les fichiers de base de données sqlite3
* table qui gère les tables SQL
* data qui gère des données au sein d'une table

### Sous-Module connect

Ce sous-module nous permet de nous connecter à une base de données ciblée.

* __Fonctions__

*
  * Ce module possède qu'une fonctions :

*
  *
    * connect() qui nous renvoie l'objet nous permettant de comuniquer avec une base de données.

* __Tests__

*
  * Les tests sont dans le module tests du projet au sein du fichier test_sqlite. On y teste notre fonction pour tenter de se connecter à une base de donnée présente dans le module de test.

* __Dépendances__

*
  * Ce module dépend du module constructeur sqlite3.

### Sous-Module bdd

Ce sous-module gérant les bases de données sqlite3 nous permet de vérifier si un fichier est bien une base de données valide et d'en créer.

* __Fonctions__

*
  * Ce module possède 2 fonctions :

*
  *
    * bdd_exist() qui vérifie qu'un fichier passé en argument est une base de donnée sqlite3 valide.
*
  *
    * bdd_create() qui crée un base de donnée sqlite3 valide à un path donné à condition qu'il pointe vers un fichier encore inexistant.

* __Tests__

*
  * Les tests sont dans le module tests du projet au sein du fichier test_sqlite. On y teste les deux fonction précédente avec divers path et une base de données déja présente dans le module de test.

* __Dépendances__

*
  * Ce module dépend du module constructeur logging et sqlite3 ainsi que le sous-module connect de ce présent module.

### Sous-Module table

Ce sous-module gérant les tables SQL nous permet de vérifier si une existe dans une base et d'en créer au sein d'une base.

* __Fonctions__

*
  * Ce module possède 2 fonctions :

*
  *
    * table_exist() qui vérifie qu'une table existe.
*
  *
    * table_create() qui crée une table selon un patern si cette table n'existe pas en base.

* __Tests__

* Les tests sont dans le module tests du projet au sein du fichier test_sqlite. On y teste nos deux fonction sur la bdd présente dans le module de test (Notamment sur la table déjà présente).

* __Dépendances__

* Ce module dépend du module constructeur logging, sqlite3.

### Sous-Module data

Ce sous-module gérant les données au sein d'une table nous permet de vérifier si une (ou des) donnée(s) existe(nt) dans une table, d'en insérer ou d'en mettre à jour.

* __Fonctions__

*
  * Ce module possède 3 fonctions :

*
  *
    * data_exist() qui vérifie qu'une donnée (ou des) existe dans une table selon un pattern de recherche.
*
  *
    * data_insert() qui insère des données dans une table selon un pattern.
*
  *
    * data_update() qui met à jour des données dans une table selon un pattern.

* __Tests__

*
  * Les tests sont dans le module tests du projet au sein du fichier test_sqlite. On y teste nos trois fonction sur la bdd présente dans le module de test (Notamment sur la table déjà présente et ses données).

* __Dépendances__

*
  * Ce module dépend du module constructeur logging, sqlite3.
