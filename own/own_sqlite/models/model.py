from ..fields import Field, Id
from ..keys import PrimaryKey, ForeignKey, Default_PrimaryKey
from ..table import table_exist, table_create
from ..data import data_extract, data_insert, data_update, data_remove, use_data
from ..models import ModelData
import sqlite3

class Model:
    """
    Classe représentant un modèle au sein d'une base de donnée.
    """

    def __init__(self, name:str, fields:list = [Id], primary_key:PrimaryKey = Default_PrimaryKey, foreign_key:list = None, db:sqlite3.Connection = None):
        """
        Cela permet d'instancier la représentation d'un modèle.
        Celle-ci requiert le nom de la base, la liste des champs du modèle,
        sa clé primaire, sa liste de clés secondaire et la base de données
        lié.
        """
        self.name = name.lower()
        self.fields = fields
        self.primary_key = primary_key
        self.foreign_key = foreign_key if foreign_key is not None else []
        if db:
            self.create(db)

    @property
    def _CreationPattern(self):
        """
        Cela permet d'obtenir le dictionnaire représentant
        la création de la table lié au modèle en base.
        """
        creationPattern = {
            "data":[],
            "primary_key":self.primary_key.pattern,
            "foreign_key":[]
        }
        for key in self.foreign_key:
            creationPattern["foreign_key"].append(key.pattern)
        for field in self.fields:
            creationPattern["data"].append(field.pattern)
        return creationPattern

    def _generateOption(self, search:dict):
        return {
            "column": search["field"].name,
            "data":{
                "format":"",
                "criteria":"%s %s" % (use_data(search["data"]), search["field"].specific_search) if search["field"].specific_search else search["data"],
                "check":False if search["field"].specific_search else True,
                "type_key":search["field"].data_type
            }
        }

    def _generateDelPattern(self, del_by:Field = None):
        """
        Cela permet d'obtenir le dictionnaire représentant
        la suppression de données au sein la table lié au modèle
        en base selon les champs données en argument.
        """
        DelPattern = {
            "table":self.name,
            "option":[],
            "seperator_option":" AND "
        }
        if del_by:
            for search in del_by:
                DelPattern["option"].append(self._generateOption(search))
        return DelPattern

    def _generateSearchPattern(self, search_by:Field = None, fields:list = None):
        """
        Cela permet d'obtenir le dictionnaire représentant
        la recherche de données au sein la table lié au modèle
        en base selon les champs données en argument.
        """
        if fields is None or len(fields) == 0:
            fields = self.fields
        SearchPattern = {
            "column":"",
            "table":self.name,
            "option":[],
            "seperator_option":" AND "
        }
        if search_by:
            for search in search_by:
                SearchPattern["option"].append(self._generateOption(search))
        for field in fields:
            if SearchPattern["column"] != "":
                SearchPattern["column"] = "%s, " % SearchPattern["column"]
            SearchPattern["column"] = "%s%s" % (SearchPattern["column"], field.name)
        return SearchPattern

    def _basePattern(self, optional_fields:list = None):
        """
        Cela permet d'obtenir la base des dictionnaire
        d'insertion ou de mise à jour.
        """
        BasePattern = {
            "table":self.name,
            "data":[],
            "string":[],
            "cp":self.primary_key.field.name
        }
        fields = []
        for field in self.fields:
            if field.required == True:
                fields.append(field)
        if optional_fields:
            for field in optional_fields:
                fields.append(field)
        return {"pattern":BasePattern, "required":fields}

    def _generateInsertionPattern(self, optional_fields:Field = None):
        """
        Cela permet d'obtenir le dictionnaire représentant
        l'insertion de données au sein la table lié au modèle
        en base avec ou non des champs optionnels.
        """
        data = self._basePattern(optional_fields)
        InsertionPattern = data["pattern"]
        fields = data["required"]
        for field in fields:
            if field.name != self.primary_key.field.name:
                InsertionPattern["data"].append(field.name)
                if field.data_type == str:
                    InsertionPattern["string"].append(field.name)
        return InsertionPattern

    def _generateUpdatePattern(self, optional_fields:Field = None):
        """
        Cela permet d'obtenir le dictionnaire représentant
        la misa à jour de données au sein la table lié au modèle
        en base avec ou non des champs optionnels.
        """
        data = self._basePattern(optional_fields)
        UpdatePattern = data["pattern"]
        fields = data["required"]
        for field in fields:
            UpdatePattern["data"].append(field.name)
            if field.data_type == str:
                UpdatePattern["string"].append(field.name)
        return UpdatePattern

    @staticmethod
    def checkInsertUpdateMany(data:list):
        """
        Cette méthode indique si les données ont été ou non
        toutes correctement insérée/ mise à jour
        """
        if data:
            out = True
            for dat in data:
                if dat == False or dat == None:
                    out = False
            return out
        return False

    def create(self, db:sqlite3.Connection):
        """
        Cette méthode créé la table au sein de la base de données
        en argument si une table du même nom d'existe pas déjà
        """
        if table_exist(self.name, db) == False:
            return table_create(self.name, self._CreationPattern, db)
        return False
    
    def _optFieldsInData(self, value:ModelData):
        optField = []
        for field in self.fields:
            if field.required is False:
                if hasattr(value, field.name):
                    optField.append(field)
        return optField


    def insertOne(self, value:ModelData, db:sqlite3.Connection):
        """
        Cette méthode permet d'insérer une donnée au sein de la base donné en argument
        """
        data = data_insert(value.pattern, self._generateInsertionPattern(self._optFieldsInData(value)), db)
        return ModelData(self.fields, data) if data is not None and data is not False else None

    def insertMany(self, value:list, db:sqlite3.Connection, verbose:bool = False):
        """
        Cette méthode permet d'insérer n donnée au sein de la base donné en argument
        """
        out = []
        for val in value:
            out.append(self.insertOne(val, db))
        if verbose == True:
            return out
        else:
            return Model.checkInsertUpdateMany(out)

    def updateOne(self, value:ModelData, db:sqlite3.Connection):
        """
        Cette méthode permet de mettre à jour une donnée au sein
        de la base donné en argument
        """
        return value if data_update(value.pattern, self._generateUpdatePattern(self._optFieldsInData(value)), db) == True else None
    
    def updateMany(self, value:list, db:sqlite3.Connection, verbose:bool = False):
        """
        Cette méthode permet de mettre à jour n donnée au sein
        de la base donné en argument
        """
        out = []
        for val in value:
            out.append(self.updateOne(val, db))
        if verbose == True:
            return out
        else:
            return Model.checkInsertUpdateMany(out)

    def returnOne(self, data:dict):
        """
        Cette méthode permet de transformer une donnée brute
        en son équivalent objet
        """
        return ModelData(self.fields, data) if len(data) > 0 else None

    def returnMany(self, data:list):
        """
        Cette méthode permet de transformer des données brute
        en leurs équivalents objets
        """
        out = []
        for dat in data:
            temp = self.returnOne(dat)
            if temp:
                out.append(temp)
        return out

    def fetchAll(self, db:sqlite3.Connection):
        """
        Cette méthode permet de récuperer toutes les données
        au sein de la base de données passé en argument
        """
        return self.returnMany(data_extract(None, self._generateSearchPattern(), db))

    def fetchOneByField(self, value:object, field:Field, db:sqlite3.Connection, comparator:str = "="):
        """
        Cette méthode permet de récuperer la première donnée
        avec le champ passé en argument avec la valeur spécifiée
        en paramètre au sein de la base de données passé en argument
        """
        if field in self.fields:
            return self.returnOne(data_extract(None, self._generateSearchPattern([{"field":field, "data":value}]), db, False, comparator))
        return None

    def fetchByField(self, value:object, field:Field, db:sqlite3.Connection, comparator:str = "="):
        """
        Cette méthode permet de récuperer toutes les données
        avec le champ passé en argument avec la valeur spécifiée
        en paramètre au sein de la base de données passé en argument
        """
        if field in self.fields:
            return self.returnMany(data_extract(None, self._generateSearchPattern([{"field":field, "data":value}]), db, comparator=comparator))
        return None

    def _extractMany(self, value:list, fields:list):
        """
        Cette méthode permet de récuperer le dictionnaire
        de recherche exploité par les méthodes exploitant
        de nombreux champs (Telles que fetchOneByFields, DelByFields
        ou encore fetchByFields)
        """
        for field in fields:
            if not (field in self.fields):
                return None
        if len(value) == len(fields):
            data = []
            for index in range(len(value)):
                data.append({
                    "field":fields[index],
                    "data":value[index]
                })
            return data

    def fetchOneByFields(self, value:list, fields:list, db:sqlite3.Connection, comparator:str = "="):
        """
        Cette méthode permet de récuperer la première donnée
        avec les champs passés en argument avec les valeur spécifiées
        en paramètre au sein de la base de données passé en argument
        """
        data = self._extractMany(value, fields)
        if data:
            return self.returnOne(data_extract(None, self._generateSearchPattern(data), db, False, comparator))
        return None
    
    def fetchByFields(self, value:list, fields:list, db:sqlite3.Connection, comparator:str = "="):
        """
        Cette méthode permet de récuperer toute les données
        avec les champs passés en argument avec les valeur spécifiées
        en paramètre au sein de la base de données passé en argument
        """
        data = self._extractMany(value, fields)
        if data:
            return self.returnMany(data_extract(None, self._generateSearchPattern(data), db, comparator=comparator))
        return None

    def delAll(self, db:sqlite3.Connection):
        """
        Cette méthode permet de supprimer toutes les données
        au sein de la base de données passé en argument
        """
        return data_remove(None, self._generateDelPattern(), db)

    def DelByField(self, value:object, field:Field, db:sqlite3.Connection):
        """
        Cette méthode permet de supprimer toutes les données
        avec le champ passé en argument avec la valeur spécifiée
        en paramètre au sein de la base de données passé en argument
        """
        if field in self.fields:
            return data_remove(None, self._generateSearchPattern([{"field":field, "data":value}]), db)
        return None

    def DelByFields(self, value:list, fields:list, db:sqlite3.Connection):
        """
        Cette méthode permet de supprimer toutes les données
        avec les champs passés en argument avec les valeur spécifiées
        en paramètre au sein de la base de données passé en argument
        """
        data = self._extractMany(value, fields)
        if data:
            return data_remove(None, self._generateDelPattern(data), db)
        return None