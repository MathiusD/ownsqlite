class ModelData:
    """
    Classe représentant un enregistrement d'un modèle.
    """

    def __init__(self, fields:list, data:tuple):
        """
        Cela permet d'instancier la représentation d'un enregistrement d'un modèle.
        Celle-ci requiert la liste des champs du modèle et le tuple contenant
        les données de l'enregistrement.
        """
        for field in fields:
            setattr(self, field.name, data[fields.index(field)])

    @property
    def pattern(self):
        """
        Cela permet d'obtenir le dictionnaire représentant l'enregistrement en base.
        """
        pattern = {}
        for key in self.__dict__.keys():
            pattern[key] = self.__dict__[key]
        return pattern