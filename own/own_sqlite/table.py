import sqlite3, logging
from own.own_sqlite import fieldTypes

def table_exist(name:str, bdd:sqlite3.Connection):
    """
    Fonction qui nous indique si une table existe bien
    dans une base de données passé en argument.\n
    Entrées :\n
    - name : chaine de caractère indiquant le nom de la table
    recherchée\n
    - bdd : une connection vers la base de données\n
    Sortie :\n
    - output : booléen indiquant si la table existe bien
    """
    output = False
    cursor = bdd.cursor()
    cursor.execute('''SELECT name FROM sqlite_master''')
    bdd.commit()
    data = cursor.fetchall()
    for result in data:
        for details in result:
            if details == name:
                output = True
    return output

def table_create(name:str, columns:dict, bdd:sqlite3.Connection):
    """
    Fonction qui créé la base nommé en argument selon le pattern
    passé lui aussi en argument sur la bdd étant le dernier
    argument. Si la table existe déjà (peut importe sa structure)
    la fonction renverra false en ne faisant rien de plus.\n
    Si celle-ci échoue elle renverra false sinon elle renverra True.\n
    Entrées :\n
    - name : chaine de caractère indiquant le nom de la table souhaitée\n
    - columns : dictionnaire indiquant la structure de la table souhaitée\n
    - bdd : une connection vers la base de données\n
    Sortie :\n
    - output : booléen indiquant si la table a été créé avec succès
    """
    output = False
    if (table_exist(name, bdd) == False):
        cursor = bdd.cursor()
        logging.debug("Request Construction")
        data = '''CREATE TABLE '''
        data = data + name +"("
        O_data = True
        for row in columns["data"]:
            if O_data == False:
                data = data + ','
            data = data + '"' + row["name"] + '" ' + row["type"]
            if row["not_null"] == "true":
                data = data + " NOT NULL"
            if columns["primary_key"]["column"] == row["name"]:
                data = data + " PRIMARY KEY"
                if columns["primary_key"]["Auto_Increment"] == "true":
                    data = data + " AUTOINCREMENT"
            if O_data == True:
                O_data = False
        for row in columns["foreign_key"]:
            if O_data == False:
                data = data + ','
            data = data + 'FOREIGN KEY("' + row["column"] + '") REFERENCES "' + row["cible"]["table"] + '"("' + row["cible"]["column"]+ '")'
            if O_data == True:
                O_data = False
        data = data + ")"
        logging.debug("Request Submit : " + data)
        try:
            cursor.execute(data)
            bdd.commit()
            output = True
        finally:
            pass
    return output

def extract_table(name:str, bdd:sqlite3.Connection):
    """
    Fonction qui extrait le dictionnaire de création d'une table,
    si elle existe.\n
    Entrées :\n
    - name : chaine de caractère indiquant le nom de la table
    recherchée\n
    - bdd : une connection vers la base de données\n
    Sortie :\n
    - output : dictionnaire de création de la table
    """
    if table_exist(name, bdd):
        cursor = bdd.cursor()
        cursor.execute('''SELECT sql FROM sqlite_master WHERE tbl_name="%s"''' % name)
        bdd.commit()
        data = cursor.fetchone()[0]
        output = {
            "data":[],
            "primary_key":None,
            "foreign_key":[]
        }
        cutData = data.split("(")
        rawFields = data[len(cutData[0]):]
        for rawField in rawFields.split(","):
            if rawField.startswith("FOREIGN KEY"):
                output["foreign_key"].append({
                    "column":rawField.split("\"")[1],
                    "cible":{
                        "table":rawField.split("\"")[3],
                        "column":rawField.split("\"")[5]
                    }
                })
            else:
                field = {
                    "name":rawField.split("\"")[1],
                    "type":None
                }
                for fieldType in fieldTypes:
                    if field["type"] is None and fieldType in rawField:
                        field["type"] = fieldType
                field["not_null"] = True if "NOT NULL" in rawField else False
                if "PRIMARY KEY" in rawField:
                    output["primary_key"] = {
                        "column":field["name"],
                        "Auto_Increment":True if "AUTOINCREMENT" in rawField else False
                    }
                output["data"].append(field)
        return output
    return None