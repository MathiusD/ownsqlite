import sqlite3
import logging
from own.own_sqlite import extract_table, typesClass

def execStatement(req:str, cursor:sqlite3.Cursor):
    """
    Fonction qui permet d'executer une requete SQL en loggant l'erreur
    qui survient si une erreur est survenue
    """
    try:
        cursor.execute(req)
    except Exception as e:
        errorLog = "Error in execution of this request : %s" % req
        logging.warning(errorLog)
        print(errorLog)
        raise e

def use_data(data:object):
    """
    Fonction qui permet d'utiliser proprement une donnée au sein
    d'une requête quel que soit le type de la donnée.\n
    Entrées : \n
    - data : La donnée à utiliser.\n
    Sortie :\n
    - req : La donnée sous forme de chaine
    """
    if data != None:
        if data.__class__ == str:
            return "\"%s\"" % data.replace("\"", "\"\"")
        else:
            return str(data)
    else:
        return "NULL"

def use_data_in_req(req:str, data:object):
    """
    Fonction qui permet d'utiliser proprement une donnée au sein
    d'une requête quel que soit le type de la donnée.\n
    Entrées : \n
    - req : La requête\n
    - data : La donnée à utiliser dans la requête.\n
    Sortie :\n
    - req : La nouvelle requête
    """
    return "%s%s" % (req, use_data(data))

def data_exist(search:tuple, search_pattern:dict, bdd:sqlite3.Connection, One_Data:bool = True):
    """
    Fonction qui nous indique si la (ou les) donnée(s) indiquée(s) est bien
    présente dans la base selon les critères de recherche
    spécifiés dans search_pattern.\n
    Entrées : \n
    - search : donnée à trouver\n
    - search_pattern : dictionnaire indiquant les critères de
    recherche\n
    - bdd : une connection vers la base de données\n
    - One_Data : booléen indiquant si on recherche plusieurs données
    ou non\n
    Sortie : \n
    - output : booléen qui indique si la donnée est présente
    """
    search = search_pattern['type_key'](search)
    output = False
    data = data_extract(search, search_pattern, bdd)
    logging.debug("Searching in Data Row")
    for result in data:
        if One_Data == True:
            for details in result:
                if search_pattern['type_key'](details) == search:
                    msg = "Data found (%s = %s)" % (search, details)
                    output = True
        else:
            if result == search:
                msg = "Data found (%s = %s)" % (search, result)
                output = True
    if output == False:
        msg = "Data Not Found in %i Entry. (Data Search is : %s)" % (len(data), search)
    logging.debug(msg)
    return output

def data_insert(data:dict, pattern:dict, bdd:sqlite3.Connection):
    """
    Fonction qui insère la donnée passé en argument selon le
    pattern donnée lui aussi en argument dans la base de données
    passée en argument également (On change pas une équipe qui
    perd). Si la donnée est effectivement insérée la fonction nous
    renvoie True, sinon elle nous renvoie False.\n
    Entrées :\n
    - data : donnée à insérer\n
    - pattern : dictionnaire qui indique la table et les champs qui
    seront spécifié pour l'insertion\n
    - bdd : une connection vers la base de données\n
    Sortie :\n
    - out : la donnée si celle-ci a été inséré sinon None\n
    """
    cursor = bdd.cursor()
    if not "cp" in pattern.keys():
        execStatement("SELECT last_insert_rowid();", cursor)
        bdd.commit()
        lastId = cursor.fetchone()
    logging.debug("Check Data")
    tableFields = extract_table(pattern["table"], bdd)
    dataIsCorrect = True
    for field in tableFields["data"]:
        if dataIsCorrect is True and field["name"] in pattern['data']:
            if not isinstance(data[field["name"]], typesClass[field["type"]]) and data[field["name"]] is not None:
                dataIsCorrect = False
    if dataIsCorrect is True:
        logging.debug("Request Construction")
        req = "INSERT INTO %s (" % pattern['table']
        first_row = True
        for row in pattern['data']:
            if first_row == False:
                req = "%s," % req
            req = "%s%s" % (req, row)
            if first_row == True:
                first_row = False
        req = "%s) VALUES(" % req
        first_row = True
        for row in pattern['data']:
            if first_row == False:
                req = "%s," % req
            req = use_data_in_req(req, data[row])
            if first_row == True:
                first_row = False
        req = "%s);" % req
        logging.debug("Request Submit : %s" % req)
        execStatement(req, cursor)
        execStatement("SELECT last_insert_rowid();", cursor)
        bdd.commit()
        id = cursor.fetchone()[0]
        if "cp" in pattern.keys():
            newPat = {
                "column":"*",
                "table":pattern["table"],
                "option":[
                    {"column":pattern["cp"],"data":{"format":"search_input","criteria":""}}
                ],
                "seperator_option":" AND ",
                "type_key":int
            }
            return data_extract(id, newPat, bdd, False)
        else:
            return True if id != lastId else False
    return False

def data_update(data:dict, pattern:dict, bdd:sqlite3.Connection):
    """
    Fonction qui met la ou les données passées en argument selon le
    pattern donnée lui aussi en argument dans la base de données
    passée en argument également (On change pas une équipe qui
    perd). Si les données sont effectivement mis à jour la fonction
    nous renvoie True, sinon elle nous renvoie False.\n
    Entrées :\n
    - data : données à insérer\n
    - pattern : dictionnaire qui indique la table et les champs qui
    seront spécifié pour la mise à jour\n
    - bdd : une connection vers la base de données\n
    Sorties :\n
    - output : booléen qui indique si les données ont été mis à jour
    """
    output = False
    cursor = bdd.cursor()
    logging.debug("Request Construction")
    req = "UPDATE " + pattern['table']
    req = req + " SET "
    first_row = True
    for row in pattern['data']:
        if first_row == False:
            req = req + ','
        req = req + row + " = "
        req = use_data_in_req(req, data[row])
        if first_row == True:
            first_row = False
    req = req + " WHERE "
    req = req + pattern['cp'] + " = "
    if pattern['cp'] in pattern['string']:
        req = req + '"' + data[pattern['cp']] + '"'
    else:
        req = req + str(data[pattern['cp']])
    logging.debug("Request Submit : " + req)
    try:
        execStatement(req, cursor)
        bdd.commit()
        output = True
    finally:
        return output

def data_extract(search:tuple, search_pattern:dict, bdd:sqlite3.Connection, All:bool = True, comparator:str = "="):
    """
    Fonction qui extrait la/les donnée(s) demandée(s)\n
    Entrées :\n
    - search : donnée à trouver\n
    - search_pattern : dictionnaire indiquant les critères de
    recherche\n
    - bdd : une connection vers la base de données\n
    - All : booléen indiquant si on recherche toutes les données
    correspondant au critère ou non\n
    Sortie :\n
    - output : Une liste des résultats
    """
    cursor = bdd.cursor()
    logging.debug("Request Construction")
    data = "SELECT " + search_pattern['column']
    data = data + " FROM " + search_pattern['table']
    first_option = True
    for row in search_pattern['option']:
        if first_option == False:
            data = data + search_pattern['seperator_option']
        else :
            data = data + " WHERE "
        data = data + row['column'] + comparator
        baseDat = search if row['data']['format'] == "search_input" else row['data']['criteria']
        if "check" in row['data'].keys() and row['data']["check"] == False:
            data = "%s%s" % (data, baseDat)
        else:
            data = use_data_in_req(data, baseDat)
        if first_option == True:
            first_option = False
    logging.debug("Request Submit : " + data)
    execStatement(data, cursor)
    bdd.commit()
    logging.debug("Extract Data Row")
    output = cursor.fetchall()
    if All == False:
        if len(output) > 0:
            output = output[0]
    logging.debug("Data : " + str(output))
    return output

def data_remove(search:tuple, del_pattern:dict, bdd:sqlite3.Connection):
    """
    Fonction qui supprime la/les donnée(s) demandée(s)\n
    Entrées :\n
    - del_pattern : dictionnaire indiquant les critères de
    recherche\n
    - bdd : une connection vers la base de données\n
    Sortie :\n
    - output : booléen qui indique si la/les donnée(s) a/ont été supprimée(s)
    """
    output = False
    cursor = bdd.cursor()
    logging.debug("Request Construction")
    data = "DELETE FROM " + del_pattern['table']
    first_option = True
    for row in del_pattern['option']:
        if first_option == False:
            data = data + del_pattern['seperator_option']
        else :
            data = data + " WHERE "
        data = data + row['column']
        if row['data']['format'] == "search_input":
            if del_pattern['type_key'] == str:
                data = data + ' = "' + search + '"'
            else:
                data = data + ' = ' + str(search)
        else:
            if row['data']['type_key'] == str:
                data = data + ' = "' + row['data']['criteria'] + '"'
            else:
                data = data + ' = ' + str(row['data']['criteria'])
        if first_option == True:
            first_option = False
    logging.debug("Request Submit : " + data)
    try:
        execStatement(data, cursor)
        bdd.commit()
        output = True
    finally:
        return output