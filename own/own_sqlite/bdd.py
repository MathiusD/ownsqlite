import logging, os
from own.own_sqlite.connect import connect

def bdd_exist(bdd_filename:str):
    """
    Fonction qui nous indique si le fichier pointé par le
    chemin donné en argument existe et que ce dernier est
    bien une base de données SQLite 3\n
    Entrée :\n
    - path: chaine de caractère indiquant le chemin vers
    la base de données\n
    Sortie :\n
    - output : booléen indiquant si le chemin pointe bien vers
    une base de données SQLite 3
    """
    output = False
    logging.debug("Call file_exist")
    if (os.path.exists(bdd_filename) == True):
        bdd = open(bdd_filename,'r', encoding = "ISO-8859-1")
        header = bdd.read(100)
        if header.startswith('SQLite format 3'):
            output = True
        bdd.close()
    return output

def bdd_create(bdd_filename:str):
    """
    Fonction qui nous créé une base de données à l'emplacement
    indiqué, si celui-ci est déjà occupé alors on arrête l'éxecution
    et on renvoie false, dans le cas où l'espace est libre on créé
    une base de données SQLite 3 à l'emplacement indiqué.\n
    Note : Afin que la base soit effectivement créé et ne soit pas
    juste un fichier vide on créé une table bidon avant de la supprimer.
    Si tout s'est bien déroulé la fonction nous renvoie True, sinon
    elle renvoie False.\n
    Entrée :\n
    - path: chaine de caractère indiquant le chemin vers
    la base de données\n
    Sortie :\n
    - output : booléen indiquant si la base a bien été créé
    """
    output = False
    logging.debug("Call file_exist")
    if (os.path.exists(bdd_filename) == False):
        try:
            logging.debug("Execute SQL Command for initiate DataBase")
            bdd = connect(bdd_filename)
            cursor = bdd.cursor()
            cursor.execute('''CREATE TABLE tests(date test)''')
            bdd.commit()
            cursor.execute('''DROP TABLE tests''')
            bdd.commit()
            bdd.close()
            output = True
        finally:
            pass
    return output